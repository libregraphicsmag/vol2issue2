# Gendering F/LOSS, Issue 2.2

## Index

* **Gender ≠ women**, ginger coon — *Editor's letter*
* **Outils Libres Alternatifs** — *Notebook*
* **Making the switch**, Manufactura Independente — *Production colophon*
* **Libre Planet 2013** — *Notebook*
* *New Releases*
* **Ticking the other box**, Antonio Roberts — *Column*
* **Rewriting hacker culture**, Eric Schrijver — *Column*
* **Consensus is sexy? The gender of collaboration**, Eleanor Greenhalgh — *Column*
* **Mothership HackerMoms: Forging Open Source community for mothers**, Aya de Leon and Sho-Sho Smith — *Dispatch*
* **Interactivos?'13, Tools for a Read-Write world**, Libre Graphics magazine team — *Dispatch*
* **Knitic**, Varvara Guljajeva and Mar Canet  — *Showcase*
* **Zone**, Ele Carpenter — *Showcase*
* **Lelacoders**, Spideralex — *Showcase*
* *Small and Useful*
* **Best of people icons** — *Best of*
* **The Empowermentors Collective — an interview**, Kýra — *Interview*
* **Modelling**, Femke Snelting — *Feature*
* *Resources & Glossary*

## Colophon

Gendering F/LOSS — January 2014
Issue 2.2, [Libre Graphics Magazine](http://libregraphicsmag.com)  
ISSN: 1925-1416