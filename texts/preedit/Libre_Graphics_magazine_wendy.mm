<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364808545771" ID="ID_1324645245" MODIFIED="1365456056816" STYLE="bubble" TEXT="My practice">
<edge COLOR="#111111"/>
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364812369734" HGAP="-5" ID="ID_1627353213" MODIFIED="1365456266093" POSITION="right" STYLE="bubble" TEXT="F/Loss" VSHIFT="30">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364813054479" HGAP="27" ID="ID_1479706219" MODIFIED="1365455871788" STYLE="bubble" TEXT="How did the biscuit come on the table: &#xa;- who made it, &#xa;- what is the recipe, &#xa;- where do the ingredients come from, &#xa;- how was it transported? " VSHIFT="-70">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364812375162" HGAP="8" ID="ID_1379147090" MODIFIED="1365456274917" POSITION="left" STYLE="bubble" TEXT="Projects" VSHIFT="78">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364811491574" HGAP="16" ID="ID_1214861" MODIFIED="1365456293464" STYLE="bubble" TEXT="Ultrasonic" VSHIFT="115">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365455675621" ID="ID_1606942027" MODIFIED="1365456242325" STYLE="bubble" TEXT="Embroidery and electronics">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364808545771" HGAP="15" ID="ID_1542522479" MODIFIED="1365456242316" STYLE="bubble" TEXT="Embroidery in history -&gt; a shift towards domesticity" VSHIFT="1">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365450550920" HGAP="30" ID="ID_1524346492" MODIFIED="1365456242317" STYLE="bubble" TEXT="A (needlework) sampler is a piece of embroidery produced as a demonstration or test of skill in needlework. [...]" VSHIFT="32">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365450330749" HGAP="26" ID="ID_786553106" MODIFIED="1365456242318" STYLE="bubble" TEXT="The oldest surviving samplers were constructed in the 15th and 16th centuries. As there were no pre-printed patterns available for needleworkers, a stitched model was needed. Whenever a needlewoman saw a new and interesting example of a stitching pattern, she would quickly sew a small sample of it onto a piece of cloth - her &apos;sampler&apos;. The patterns were sewn randomly onto the fabric as a reference for future use, and the woman would collect extra stitches and patterns throughout her lifetime.&#xa;&#xa;16th Century English samplers were stitched on a narrow band of fabric 6&#x2013;9 in (150&#x2013;230 mm) wide. As fabric was very expensive, these samplers were totally covered with stitches. These were known as band samplers and valued highly, often being mentioned in wills and passed down through the generations. These samplers were stitched using a variety of needlework styles, threads, and ornament. Many of them were exceedingly elaborate, incorporating subtly shaded colours, silk and metallic embroidery threads, and using stitches such as Hungarian, Florentine, tent, cross, long-armed cross, two-sided Italian cross, rice, running, Holbein, Algerian eye and buttonhole stitches. The samplers also incorporated small designs of flowers and animals, and geometric designs stitched using as many as 20 different colors of thread.&#xa;&#xa;The first printed pattern book was produced in 1523, but they were not easily obtainable and a sampler was the most common form of reference available to many women.&#xa;(excellent Wikipedia page on the Sampler)" VSHIFT="16">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365452677664" ID="ID_670974559" MODIFIED="1365456242323" STYLE="bubble" TEXT="A shift in importance, from knowledge transfer to knowledge demonstration">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365450527355" HGAP="16" ID="ID_1844641657" MODIFIED="1365456242323" STYLE="bubble" TEXT="By the 18th century, samplers were a complete contrast to the scattered samples sewn earlier on. These samplers were stitched more to demonstrate knowledge than to preserve skill. The stitching of samplers was believed to be a sign of virtue, achievement and industry, and girls were taught the art from a young age." VSHIFT="-6">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365452115772" HGAP="19" ID="ID_728601373" MODIFIED="1365456242324" STYLE="bubble" TEXT="At the same time, there is a great shift towards domesticity, the importance of the home, piousness. " VSHIFT="-180">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1364814066851" HGAP="-6" ID="ID_1534281163" MODIFIED="1365456288374" STYLE="bubble" TEXT="Social Protocols" VSHIFT="-299">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448013908" HGAP="28" ID="ID_874342143" MODIFIED="1365456259634" POSITION="right" STYLE="bubble" TEXT="Encountered Gender Biasses" VSHIFT="52">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448021901" ID="ID_1163340116" MODIFIED="1365451928455" STYLE="bubble" TEXT="&quot;OMG a girl!&quot;">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448051107" ID="ID_1946984740" MODIFIED="1365451928455" STYLE="bubble">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      ?<i>&#160;geek == man </i>?
    </p>
  </body>
</html></richcontent>
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448298237" ID="ID_1729642864" MODIFIED="1365451928456" STYLE="bubble" TEXT="Invisibility">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448306311" ID="ID_1210132026" MODIFIED="1365455887246" STYLE="bubble" TEXT="Underestimated">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448343271" ID="ID_243551822" MODIFIED="1365451928456" STYLE="bubble" TEXT="Brain blablabla">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448387187" ID="ID_1385639252" MODIFIED="1365451928456" STYLE="bubble" TEXT="&quot;They&apos;re just not interested&quot;">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448420080" ID="ID_1807617894" MODIFIED="1365451928456" STYLE="bubble" TEXT="Biological essentialism">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365449851468" HGAP="37" ID="ID_1987967972" MODIFIED="1365876668497" POSITION="left" STYLE="bubble" TEXT="Craftwomanship" VSHIFT="-133">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365449730745" HGAP="82" ID="ID_969037778" MODIFIED="1365455995664" STYLE="bubble" TEXT="I opened up my first machine when I was 27 years old, learnt how to solder at 29 &#xa;(microphone and adio cables)" VSHIFT="36">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365448810832" ID="ID_1576712084" MODIFIED="1365451928456" STYLE="bubble" TEXT="Programming, like sewing, is largely a &quot;tacit&quot; skill, &#xa;which is best learned by doing and by watching others">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365450018620" ID="ID_1207102591" MODIFIED="1365451928458" STYLE="bubble" TEXT="Fora - blogposts - mailinglists - websites - tutorials - video&apos;s - comments - books - humans &#xa;trial and error - trial and success&#xa;repetition - documentation &#xa;autodidact">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365450278639" ID="ID_1938730834" MODIFIED="1365451928460" STYLE="bubble" TEXT="again and again and again">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456066758" HGAP="0" ID="ID_608939809" MODIFIED="1365456257239" POSITION="right" STYLE="bubble" TEXT="Open Hardware" VSHIFT="-100">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456121551" HGAP="42" ID="ID_1380719875" MODIFIED="1365456242283" STYLE="bubble" TEXT="microcontrollers" VSHIFT="20">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456131271" HGAP="40" ID="ID_1758312990" MODIFIED="1365456242283" STYLE="bubble" TEXT="sensors" VSHIFT="-1">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456137641" HGAP="15" ID="ID_522485704" MODIFIED="1365456242283" STYLE="bubble" TEXT="hand made sensors" VSHIFT="1">
<font NAME="Linux Libertine O" SIZE="14"/>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456146945" ID="ID_1590124481" MODIFIED="1365456242283" STYLE="bubble" TEXT="embroidered">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456153280" ID="ID_887077183" MODIFIED="1365456242283" STYLE="bubble" TEXT="knit">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456158210" ID="ID_416586295" MODIFIED="1365456242283" STYLE="bubble" TEXT="sewn">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
</node>
<node BACKGROUND_COLOR="#ffffff" COLOR="#000000" CREATED="1365456169724" ID="ID_180895773" MODIFIED="1365456242283" STYLE="bubble" TEXT="licenses">
<font NAME="Linux Libertine O" SIZE="14"/>
</node>
</node>
</node>
</map>
