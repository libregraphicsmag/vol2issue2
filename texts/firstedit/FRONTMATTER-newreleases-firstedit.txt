New releases

L’Ève future – Spécimens de fontes libres
From Greyscale Press comes a book packed full of gorgeous, permissively licensed type specimens. An indispensable tool for designers considering F/LOSS fonts.
http://greyscalepress.com/2013/books/eve-future-specimens-de-fontes-libres/

Fait Main magazine, Volume 3
An almanac of all things homebrew (in some cases, quite literally), the current issue of //Fait Main// offers lessons about stereos, soap and yes, beer.
http://faitmain.org/volume-2/edito.html

Calligra
The KDE application suite is seeing fast progress: the 2.7.4 stable version is just out, and the 2.8 beta is available if you want a taste of the bleeding edge.
http://www.calligra.org/get-calligra/

Fedora 20
Following a tradition of funky version naming, the twentieth major release of Red Hat's community distro is known as "Heisenbug," and comes with sweet new things like more powerful networking or thin-client and ARM support. And the release coincides with Fedora's 10th birthday.
http://fedoraproject.org

The British Library on Flickr Commons
A trove of illustrations spanning 3 centuries was released into the Public Domain by the British Library. It was also announced that there will be a crowdsourced platform for tagging and identifying the images in 2014.
http://britishlibrary.typepad.co.uk/digital-scholarship/2013/12/a-million-first-steps.html
http://www.flickr.com/photos/britishlibrary

Multimedia programming with Pure Data
Pure Data is a very powerful tool for visual coding, but its learning curve might be too steep for some. This book comes to clarify and demonstrate how to use PD for audio-visual performances, interactive displays, animation and interface design.
http://www.packtpub.com/multimedia-programming-with-pure-data/book

AlphabeNt
Beautifully printed and put together, this publication by Drew Taylor and Daniel Purvis presents an alternative view of the latin alphabet from a glitch art perspective.
http://stolen-projects.myshopify.com/collections/frontpage/products/alphabent-experiments-from-a-z-book-1

Considering your tools
Introduced as "a reader for designers and developers," Considering your Tools responds to a dearth of literature around the use of digital tools in graphic design. An extensive and diverse set of texts and essays around language, gestures, devices and tools.
http://reader.lgru.net/pages/index/

Subtle patterns
Simple, well designed and indeed subtle patterns, available with a Creative Commons Attribution-ShareAlike license.
http://subtlepatterns.com/

Stdin Bazar
The Brussels-based design studio opened up their cupboards, sharing their homebrewed HOWTO's, recipes and guides for a number of purposes, ranging from command-line image and video manipulation to database and typesetting software tips. All available under the Free Art License.
http://stdin.fr/Bazar/

